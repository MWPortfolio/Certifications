# Certifications



# Coursera  Certifications/Description

    https://www.coursera.org/learn/front-end-react/home/welcome 


    https://www.coursera.org/specializations/full-stack-react/enroll'
   

Gits:


https://github.com/xAirx/Coursera-UniversityofHK-React


https://github.com/xAirx/Coursera-UniversityofHK-ReactNative


https://github.com/xAirx/Coursera-UniversyofHK-ServerSideNode



### Course1

https://www.coursera.org/learn/front-end-react/home/welcome

    Front-End Web Development with React


This course explores Javascript based front-end application development, and in particular the React library (Currently Ver. 16.3). This course will use JavaScript ES6 for developing React application. You will also get an introduction to the use of Reactstrap for Bootstrap 4-based responsive UI design. You will be introduced to various aspects of React components. You will learn about React router and its use in developing single-page applications. You will also learn about designing controlled forms. You will be introduced to the Flux architecture and Redux. You will explore various aspects of Redux and use it to develop React-Redux powered applications. You will then learn to use Fetch for client-server communication and the use of REST API on the server side. A quick tour through React animation support and testing rounds off the course. You must have preferably completed the previous course in the specialization on Bootstrap 4, or have a working knowledge of Bootstrap 4 to be able to navigate this course. Also a good working knowledge of JavaScript, especially ES 5 is strongly recommended. At the end of this course you will: - Be familiar with client-side Javascript application development and the React library - Be able to implement single page applications in React - Be able to use various React features including components and forms - Be able to implement a functional front-end web application using React - Be able to use Reactstrap for designing responsive React applications - Be able to use Redux to design the architecture for a React-Redux application


### Course2

https://www.coursera.org/learn/react-native

    Multiplatform Mobile App Development with React Native

    This course focuses on developing truly cross-platform, native iOS and Android apps using React Native (Ver 0.55) and the Expo SDK (Ver. 27.0.0). React Native uses modern JavaScript to get truly native UI and performance while sharing skills and code with the web. You will learn about UI development with React Native UI and layout support and access the native mobile platform's capabilities from Javascript using the Expo SDK. You should have already completed the Bootstrap 4 and the React courses in this specialization before proceeding with this course. At the end of this course you will be able to (a) Build mobile applications targeting multiple platforms with a single codebase, (b) Leverage your React and Javascript skills, (c) Use various features of React Native and the Expo SDK to build truly cross-platform mobile applications, and (d) Use Redux to design the architecture for a React-Redux application
    
    
### Course3

https://www.coursera.org/learn/server-side-nodejs

        Server-side Development with NodeJS, Express and MongoDB


        This course deals with all things server-side. We base the entire course around the NodeJS platform. We start with a brief overview of the Web protocols: HTTP and HTTPS. We examine NodeJS and NodeJS modules: Express for building web servers. On the database side, we review basic CRUD operations, NoSQL databases, in particular MongoDB and Mongoose for accessing MongoDB from NodeJS. We examine the REST concepts and building a RESTful API. We touch upon authentication and security. Finally we review backend as a service (BaaS) approaches, including mobile BaaS, both open-source and commercial BaaS services. At the end of this course, you will be able to: - Demonstrate an understanding of server-side concepts, CRUD and REST - Build and configure a backend server using NodeJS framework - Build a RESTful API for the front-end to access backend services



## Freecodecamp


            - Finish all relevant certifications

                Responsive web design certification

                Javascript Algorithms and Data structures certification

                Front end Libraries Certification

                Apis and Microservices Certification


                Information security and Quality Assurance Certification



                        ###-> Nodeschool

                             Node School (Re-readup and new learning about node etc). - TODO
                             Node.js streams

                             https://www.guru99.com/node-js-streams-filestream-pipes.html

                             https://www.quora.com/What-are-the-best-resources-for-learning-Node-js

                             https://www.w3schools.com/nodejs/nodejs_intro.asp

                             Finish these:

                             https://github.com/maxogden/art-of-node/#the-art-of-node

                             https://nodeschool.io/#workshoppers

                             https://www.vskills.in/practice/nodejs




   ## Freecodecamp Progress.


                        ## Javascript CSS // SASS // PRACTICE // BRUSHUP.  - done

                            https://www.educba.com/javascript-vs-jquery/

                            https://www.codecademy.com/learn/introduction-to-javascript
                            #READUP // Brushup.


                        &nbsp;&nbsp;&nbsp;&nbsp;

                                # Hardcore hands on "basics" "re-stick" phase
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;

                                # Phase 1 Responsive Web Design Certification

                                #### jQuery

                                https://www.codecademy.com/learn/learn-jquery


                                #### CSS

                                https://learnlayout.com/toc.html


                                #### SASS

                                https://www.codecademy.com/learn/learn-sass

                                &nbsp;&nbsp;&nbsp;&nbsp;


                                #### MEMRISE
                                https://www.memrise.com/course/700033/learn-css/


                        #### Css basics and nail them  - done

                              Make the rulebook and make it stick!!
                              https://university.webflow.com
                              https://www.youtube.com/watch?v=I9XRrlOOazo&list=PL4cUxeGkcC9gQeDH6xYhmO-db2mhoTSrT
                              https://medium.com/ux-art/html-block-and-inline-elements-b65775e86599
                              https://medium.com/launch-school/https-medium-com-dembasiby-understanding-the-css-box-model-b005a82593a6

                              &nbsp;&nbsp;&nbsp;&nbsp;


                        #### Finish the Responsive web design section on Freecodecamp   - done

                              #### Do FrontendFrameworks on Freecodecamp aswell except for react. -> Css grid vs flexbox vs bootstrap

                              https://medium.com/youstart-labs/beginners-guide-to-choose-between-css-grid-and-flexbox-783005dd2412

                              https://hackernoon.com/the-ultimate-css-battle-grid-vs-flexbox-d40da0449faf

                              https://css-tricks.com/css-grid-replace-flexbox/

                              https://www.youtube.com/watch?v=x7tLPhnA06w

                              https://css-tricks.com/snippets/css/a-guide-to-flexbox/

                              https://css-tricks.com/snippets/css/complete-guide-grid/

                              https://university.webflow.com/courses/grid


                                    Flexbox Froggy
                                    A game to learn Flexbox


                                    Flexbox Zombies
                                    A course to learn Flexbox

                                    CSS Gridgarden
                                    A game to learn Grid


                        &nbsp;&nbsp;&nbsp;&nbsp;


                        &nbsp;&nbsp;&nbsp;&nbsp;


                        &nbsp;&nbsp;&nbsp;&nbsp;


                        &nbsp;&nbsp;&nbsp;&nbsp;



                        ## Phase 2  Javascript Algorithms And Data Structures Certification 


                                https://learn.freecodecamp.org/.  - done


                           ### Extra 

                                https://www.memrise.com/course/700034/learn-javascript/1/?action=prev

                                https://warriorjs.com/warriors/new?ref=docs

                                https://www.youtube.com/watch?v=4l3bTDlT6ZI

                                https://www.youtube.com/playlist?list=PL4cUxeGkcC9haFPT7J25Q9GRB_ZkFrQAc



                        #### More JavaScript   - done

                              https://codeburst.io/a-simple-guide-to-destructuring-and-es6-spread-operator-e02212af5831
                              https://gist.github.com/mikaelbr/9900818

                              https://blog.bitsrc.io/understanding-higher-order-functions-in-javascript-75461803bad

                              https://www.freecodecamp.org/news/a-quick-intro-to-higher-order-functions-in-javascript-1a014f89c6b/

                              https://www.google.com/search?client=firefox-b-d&ei=IStQXbmzDZLnsAf7sb6wCQ&q=higher+order+functions+javascript&oq=higher+order+functions+javascript&gs_l=psy-ab.3..0l10.6939.11256..11395...5.0..0.132.3024.33j5......0....1..gws-wiz.......0i71j0i67j0i273j0i13j33i160.BvPMMWHex6c&ved=0ahUKEwj5x-6wiPvjAhWSM-wKHfuYD5YQ4dUDCAo&uact=5

                              https://www.sitepoint.com/higher-order-functions-javascript/

                              https://www.youtube.com/watch?v=rRgD1yVwIvE

                              http://www.discovermeteor.com/blog/understanding-javascript-map/

                              https://codeburst.io/learn-understand-javascripts-filter-function-bde87bce206

                              https://codeburst.io/learn-understand-javascripts-reduce-function-b2b0406efbdc

                              https://codeburst.io/javascript-learn-to-chain-map-filter-and-reduce-acd2d0562cd4

                              https://codeburst.io/javascript-prototype-cb29d82b8809

                              https://codeburst.io/master-javascript-prototypes-inheritance-d0a9a5a75c4e.


                        &nbsp;&nbsp;&nbsp;&nbsp;


                        &nbsp;&nbsp;&nbsp;&nbsp;


                        &nbsp;&nbsp;&nbsp;&nbsp;


                        &nbsp;&nbsp;&nbsp;&nbsp;



                        ## Phase 3 Front End Libraries Certification 


                        ### Webgems
                              https://webgems.io/



                        #### Learning-MERN-Stack


                        #### Codeacademy  - done
                              https://www.codecademy.com/learn/react-101  
                              https://www.codecademy.com/learn/react-102  


                        ##### React part of freecodecamp  - done

                              Essential Reading: Learn React from Scratch! (2019 Edition) 
                              https://scotch.io/starters/react?utm_content=in-content

                              https://learn.freecodecamp.org/ 



&nbsp;&nbsp;&nbsp;&nbsp;

&nbsp;&nbsp;&nbsp;&nbsp;

&nbsp;&nbsp;&nbsp;&nbsp;

&nbsp;&nbsp;&nbsp;&nbsp;
